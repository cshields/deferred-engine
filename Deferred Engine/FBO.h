#pragma once

#include <glew.h>
#include <vector>
#include <unordered_map>

class FBO
{
protected:
	std::vector<GLenum>* m_buffers;

	std::unordered_map<char*, GLuint> m_colourAttachments;
	GLuint m_depth;
	GLuint m_handle;

public:
	FBO();
	~FBO();

	void createRenderBuffer(GLenum type, GLuint width, GLuint height);
	void createAttachment(char* name, GLenum format , GLuint width, GLuint height);
	void drawBuffers();

	GLuint getHandle() { return m_handle; }
	GLuint getDepth() { return m_depth; }
	GLuint getAttachment(char* name) { return m_colourAttachments[name]; }
};

