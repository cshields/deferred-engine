#pragma once
#include <glew.h>
#include <glfw3.h>
class Window
{
private:

	GLFWwindow* window;

	static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
#ifdef _DEBUG
	static void error_callback(int error, const char* description);
	static void GLError(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, void* userParam);
#endif // DEBUG

public:
	Window();
	~Window();

	void init();

	GLFWwindow* getWindow() { return window; }
};

