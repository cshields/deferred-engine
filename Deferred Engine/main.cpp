#include "renderer.h"
#include "Window.h"


int main()
{
	Window window;
	window.init();

	Renderer renderer;
	renderer.init();

	renderer.draw(window.getWindow());

	return 0;
}