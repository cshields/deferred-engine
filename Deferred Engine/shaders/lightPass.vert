#version 430

layout(location = 0) in vec4 in_position;
layout(location = 2) in vec2 in_texCoord;

out vec2 texCoord;;

void main()
{
	texCoord = in_texCoord;
	gl_Position = in_position;
}

