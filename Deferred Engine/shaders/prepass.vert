#version 430

layout(location = 0) in vec4 in_position;

layout(location = 0) uniform mat4 uni_MVP;

void main()
{
	gl_Position = uni_MVP * in_position;
}