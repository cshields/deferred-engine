#version 430

layout(location = 0) in vec4 in_position;
layout(location = 1) in vec3 in_normal;

out vec4 position;
out vec3 normal;

layout(location = 0) uniform mat4 uni_MVP;
layout(location = 1) uniform mat4 uni_modelView;
layout(location = 2) uniform mat3 uni_normalMat;


void main()
{
	position = uni_modelView * in_position;
	normal = normalize(uni_normalMat * in_normal);
	
	gl_Position = uni_MVP * in_position;
}

