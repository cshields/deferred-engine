#version 430

in vec4 position;
in vec3 normal;

layout(location = 0) out vec4 fragPosition;
layout(location = 1) out vec3 fragNormal;
layout(location = 2) out vec4 fragColour;

void main()
{
	fragPosition = position;
	fragNormal = normal;
	fragColour = vec4(1.0);
}