#version 430

in vec2 texCoord;

layout(location = 0) out vec4 fragColour;

layout(binding = 0) uniform sampler2D tex_position;
layout(binding = 1) uniform sampler2D tex_normal;
layout(binding = 2) uniform sampler2D tex_colour;

void main()
{
    vec4 position = texture(tex_position, texCoord);
    vec3 normal = texture(tex_normal, texCoord).rgb;
    vec4 colour = texture(tex_colour, texCoord);
    
	vec4 ambient = colour * 0.1; 
    
    float exposure = dot(normalize(-position.rgb), normal);
    vec4 diffuse = colour * exposure;
    
    vec4 specular = vec4(0.0);
    if (exposure > 0.0){
        vec3 R = reflect(normalize(position.rgb), normal);
        specular = vec4(1.0) * pow(max(dot(R, normalize(-position.rgb)), 0.0), 10.0);
    }
    
	fragColour = max(ambient, diffuse) + specular;
}