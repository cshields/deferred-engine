#pragma once

#define VERTEX		0
#define NORMAL		1
#define TEXCOORD    2
#define INDEX		3

#include <Importer.hpp>
#include <scene.h>           // Output data structure
#include <postprocess.h>     // Post processing flags
#include <glew.h>
#include <glm.hpp>
#include <fstream>
#include <string>
#include <iostream>
#include <vector>

using std::vector;

namespace glt {

	struct lightStruct {
		GLfloat ambient[4];
		GLfloat diffuse[4];
		GLfloat specular[4];
	};

	struct materialStruct {
		GLfloat ambient[4];
		GLfloat diffuse[4];
		GLfloat specular[4];
		GLfloat shininess;
	};

	void exitFatalError(const char *message) {
		std::cout << message << " ";
		exit(1);
	}

	// loadFile - loads text file from file fname as a char* 
	// Allocates memory - so remember to delete after use
	// size of file returned in fSize
	char* loadFile(const char *fname, GLint &fSize) {
		int size;
		char * memblock;

		// file read based on example in cplusplus.com tutorial
		// ios::ate opens file at the end
		std::ifstream file(fname, std::ios::in | std::ios::binary | std::ios::ate);
		if (file.is_open()) {
			size = (int)file.tellg(); // get location of file pointer i.e. file size
			fSize = (GLint)size;
			memblock = new char[size];
			file.seekg(0, std::ios::beg);
			file.read(memblock, size);
			file.close();
			std::cout << "file " << fname << " loaded" << std::endl;
		}
		else {
			std::cout << "Unable to open file " << fname << std::endl;
			fSize = 0;
			// should ideally set a flag or use exception handling
			// so that calling function can decide what to do now
			return nullptr;
		}
		return memblock;
	}


	GLuint initShader(const char *vertFile, const char *fragFile) {

		GLuint vertShader, fragShader, program;
		vertShader = glCreateShader(GL_VERTEX_SHADER);
		fragShader = glCreateShader(GL_FRAGMENT_SHADER);

		if (0 == vertShader)
		{
			fprintf(stderr, "Error creating vertex shader.\n");
			exit(1);
		}

		if (0 == fragShader)
		{
			fprintf(stderr, "Error creating frag shader.\n");
			exit(1);
		}


		GLint vLen, fLen;
		const GLchar *vertCode, *fragCode, *compCode;
		vertCode = loadFile(vertFile, vLen);
		fragCode = loadFile(fragFile, fLen);
		glShaderSource(vertShader, 1, &vertCode, &vLen);
		glShaderSource(fragShader, 1, &fragCode, &fLen);

		glCompileShader(vertShader);
		GLint result;
		glGetShaderiv(vertShader, GL_COMPILE_STATUS, &result);
		if (GL_FALSE == result)
		{
			fprintf(stderr, "Vertex shader compilation failed.\n");
			GLint logLen;
			glGetShaderiv(vertShader, GL_INFO_LOG_LENGTH, &logLen);
			if (logLen > 0)
			{
				char *log = (char *)malloc(logLen);
				GLsizei written;
				glGetShaderInfoLog(vertShader, logLen, &written, log);
				fprintf(stderr, "Shader log:\n%s", log);
				free(log);
			}
		}

		glCompileShader(fragShader);
		glGetShaderiv(fragShader, GL_COMPILE_STATUS, &result);
		if (GL_FALSE == result)
		{
			fprintf(stderr, "Frag shader compilation failed.\n");
			GLint logLen;
			glGetShaderiv(fragShader, GL_INFO_LOG_LENGTH, &logLen);
			if (logLen > 0)
			{
				char *log = (char *)malloc(logLen);
				GLsizei written;
				glGetShaderInfoLog(fragShader, logLen, &written, log);
				fprintf(stderr, "Shader log:\n%s", log);
				free(log);
			}
		}

		program = glCreateProgram();
		if (0 == program)
		{
			fprintf(stderr, "Error creating program object.\n");
			exit(1);
		}

		glAttachShader(program, vertShader);
		glAttachShader(program, fragShader);

		glLinkProgram(program);
		glGetProgramiv(program, GL_LINK_STATUS, &result);
		if (GL_FALSE == result)
		{
			fprintf(stderr, "Failed to link shader program!\n");
			GLint logLen;
			glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLen);
			if (logLen > 0)
			{
				char *log = (char *)malloc(logLen);
				GLsizei written;
				glGetShaderInfoLog(program, logLen, &written, log);
				fprintf(stderr, "Program log:\n%s", log);
				free(log);
			}
			else
				glUseProgram(program);
		}

		delete[] vertCode;
		delete[] fragCode;

		return program;
	}

	GLuint createComputeShader(const char *compFile) {
		GLuint compShader, program;
		compShader = glCreateShader(GL_COMPUTE_SHADER);

		if (0 == compShader)
		{
			fprintf(stderr, "Error creating comp shader.\n");
			exit(1);
		}

		const GLchar *compCode;
		GLint cLen;
		compCode = loadFile(compFile, cLen);
		glShaderSource(compShader, 1, &compCode, &cLen);

		glCompileShader(compShader);
		GLint result;
		glGetShaderiv(compShader, GL_COMPILE_STATUS, &result);
		if (GL_FALSE == result)
		{
			fprintf(stderr, "Comp shader compilation failed.\n");
			GLint logLen;
			glGetShaderiv(compShader, GL_INFO_LOG_LENGTH, &logLen);
			if (logLen > 0)
			{
				char *log = (char *)malloc(logLen);
				GLsizei written;
				glGetShaderInfoLog(compShader, logLen, &written, log);
				fprintf(stderr, "Shader log:\n%s", log);
				free(log);
			}
		}

		program = glCreateProgram();
		if (0 == program)
		{
			fprintf(stderr, "Error creating program object.\n");
			exit(1);
		}

		glAttachShader(program, compShader);

		glLinkProgram(program);
		glGetProgramiv(program, GL_LINK_STATUS, &result);
		if (GL_FALSE == result)
		{
			fprintf(stderr, "Failed to link shader program!\n");
			GLint logLen;
			glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLen);
			if (logLen > 0)
			{
				char *log = (char *)malloc(logLen);
				GLsizei written;
				glGetShaderInfoLog(program, logLen, &written, log);
				fprintf(stderr, "Program log:\n%s", log);
				free(log);
			}
			else
				glUseProgram(program);
		}
		delete[] compCode;

		return program;
	}

	void setUniform(const GLuint program, const int uniformIndex, const glm::mat4 data) { glUniformMatrix4fv(uniformIndex, 1, GL_FALSE, &data[0][0]); }
	void setUniform(const GLuint program, const int uniformIndex, const glm::mat3 data) { glUniformMatrix3fv(uniformIndex, 1, GL_FALSE, &data[0][0]); }
	void setUniform(const GLuint program, const int uniformIndex, const glm::vec4 data) { glUniform4fv(uniformIndex, 1, &data[0]); }
	void setUniform(const GLuint program, const int uniformIndex, const glm::vec3 data) { glUniform3fv(uniformIndex, 1, &data[0]); }
	void setUniform(const GLuint program, int uniformIndex, const glm::vec2 data) { glUniform2fv(uniformIndex, 1, &data[0]); }
	void setUniform(const GLuint program, const int uniformIndex, const GLfloat data) { glUniform1fv(uniformIndex, 1, &data); }
	void setUniform(const GLuint program, const int uniformIndex, const int data) { glUniform1iv(uniformIndex, 1, &data); }

	void drawMesh(const GLuint mesh, const GLuint indexCount, const GLuint primitive) {
		glBindVertexArray(mesh);	// Bind mesh VAO
		glDrawElements(primitive, indexCount, GL_UNSIGNED_INT, 0);	// draw VAO 
	}

	void setLight(const GLuint program, const lightStruct light) {
		// pass in light data to shader
		int uniformIndex = glGetUniformLocation(program, "light.ambient");
		glUniform4fv(uniformIndex, 1, light.ambient);
		uniformIndex = glGetUniformLocation(program, "light.diffuse");
		glUniform4fv(uniformIndex, 1, light.diffuse);
		uniformIndex = glGetUniformLocation(program, "light.specular");
		glUniform4fv(uniformIndex, 1, light.specular);
	}


	void setMaterial(const GLuint program, const materialStruct material) {
		// pass in material data to shader 
		int uniformIndex = glGetUniformLocation(program, "material.ambient");
		glUniform4fv(uniformIndex, 1, material.ambient);
		uniformIndex = glGetUniformLocation(program, "material.diffuse");
		glUniform4fv(uniformIndex, 1, material.diffuse);
		uniformIndex = glGetUniformLocation(program, "material.specular");
		glUniform4fv(uniformIndex, 1, material.specular);
		uniformIndex = glGetUniformLocation(program, "material.shininess");
		glUniform1f(uniformIndex, material.shininess);
	}


	GLuint importMesh(char* filename, GLuint *size, GLuint pFlags = aiProcess_Triangulate | aiProcess_JoinIdenticalVertices)
	{
		Assimp::Importer importer;
		const aiScene* mesh = importer.ReadFile(filename, pFlags);

		GLuint VAO;
		// generate and set up a VAO for the mesh
		glGenVertexArrays(1, &VAO);
		glBindVertexArray(VAO);

		GLuint VBO;
		glGenBuffers(1, &VBO);

		vector<glm::vec3> v;
		vector<glm::vec2> t;
		vector<glm::vec3> n;
		vector<unsigned int> indices;
		vector<glm::vec3> tan;
		vector<glm::vec3> bitan;

		for (int i = 0; i < mesh->mMeshes[0]->mNumVertices; i++){
			aiVector3D* verts = &mesh->mMeshes[0]->mVertices[i];
			v.push_back(glm::vec3(verts->x, verts->y, verts->z)); // vertex buffer

			if (mesh->mMeshes[0]->HasNormals() == true){
				aiVector3D* norms = &mesh->mMeshes[0]->mNormals[i];
				n.push_back(glm::vec3(norms->x, norms->y, norms->z)); // normal buffer
			}

			if (mesh->mMeshes[0]->HasTextureCoords(0) == true){
				aiVector3D* tex_coords = &mesh->mMeshes[0]->mTextureCoords[0][i]; // UV buffer
				t.push_back(glm::vec2(tex_coords->x, tex_coords->y));
			}

			if (mesh->mMeshes[0]->HasTangentsAndBitangents() == true){
				aiVector3D* tangents = &mesh->mMeshes[0]->mTangents[i];
				tan.push_back(glm::vec3(tangents->x, tangents->y, tangents->z)); // tangent buffer

				aiVector3D* bitangents = &mesh->mMeshes[0]->mBitangents[i];
				bitan.push_back(glm::vec3(bitangents->x, bitangents->y, bitangents->z)); // bitangent buffer
			}
		}

		*size = mesh->mMeshes[0]->mNumFaces * 3;

		for (unsigned int i = 0; i < mesh->mMeshes[0]->mNumFaces; i++) {
			const aiFace& Face = mesh->mMeshes[0]->mFaces[i];
			assert(Face.mNumIndices == 3);
			indices.push_back(Face.mIndices[0]);
			indices.push_back(Face.mIndices[1]);
			indices.push_back(Face.mIndices[2]);
		}

		// VBO for vertex data
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, 3 * mesh->mMeshes[0]->mNumVertices*sizeof(GLfloat), v.data(), GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);

		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, 3 * mesh->mMeshes[0]->mNumVertices*sizeof(GLfloat), n.data(), GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(1);

		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, 2 * mesh->mMeshes[0]->mNumVertices*sizeof(GLfloat), t.data(), GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)2, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(2);

		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, 3 * mesh->mMeshes[0]->mNumVertices*sizeof(GLfloat), tan.data(), GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)3, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(3);

		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, 3 * mesh->mMeshes[0]->mNumVertices*sizeof(GLfloat), bitan.data(), GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)4, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(4);

		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * mesh->mMeshes[0]->mNumFaces*sizeof(GLuint), indices.data(), GL_STATIC_DRAW);

		return VAO;
	}

	GLuint generateQuad()
	{
		const glm::vec3 positions[] { glm::vec3(1.f, 1.f, -1.f),
										glm::vec3(1.f, -1.f, -1.f),
										glm::vec3(-1.f, -1.f, -1.f),
										glm::vec3(-1.f, 1.f, -1.f) };

		const glm::vec2 texCoords[] { glm::vec2(1.f, 1.f),
										glm::vec2(1.f, 0.f),
										glm::vec2(0.f, 0.f),
										glm::vec2(0.f, 1.f) };

		const glm::uvec3 indices[]	{ glm::uvec3(2, 1, 0),
										glm::uvec3(2, 0, 3) };

		GLuint VBO[3], VAO;
		// generate vertex array
		glGenVertexArrays(1, &VAO);
		glBindVertexArray(VAO);

		// generate 3 buffers
		glGenBuffers(3, VBO);

		// positions
		glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
		glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(glm::vec3), positions, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
		glEnableVertexAttribArray(0);

		// texture coordinates
		glBindBuffer(GL_ARRAY_BUFFER, VBO[1]);
		glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(glm::vec2), texCoords, GL_STATIC_DRAW);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
		glEnableVertexAttribArray(2);

		// indices
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO[2]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, 2 * sizeof(glm::uvec3), indices, GL_STATIC_DRAW);

		return VAO;
	}

};
