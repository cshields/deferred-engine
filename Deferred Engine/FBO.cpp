#include "FBO.h"
#include <assert.h>
#include <iostream>


FBO::FBO()
{
	m_buffers = new std::vector<GLenum>;
	glGenFramebuffers(1, &m_handle);
	glBindFramebuffer(GL_FRAMEBUFFER, m_handle);
}


FBO::~FBO()
{
}

void FBO::createRenderBuffer(GLenum type, GLuint width, GLuint height)
{
	glGenRenderbuffers(1, &m_depth);
	glBindRenderbuffer(GL_RENDERBUFFER, m_depth);
	glRenderbufferStorage(GL_RENDERBUFFER, type, width, height);

	if (type == GL_DEPTH_COMPONENT)
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_depth);

	//m_buffers->push_back(GL_NONE);
}

void FBO::createAttachment(char* name, GLenum format, GLuint width, GLuint height)
{
	GLuint att;
	// create render target
	glGenTextures(1, &att);
	glBindTexture(GL_TEXTURE_2D, att);
	m_colourAttachments.emplace(name, att);

	// allocate space in memory
	glTexStorage2D(GL_TEXTURE_2D, 1, format, width, height);

	// set appropriate parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	// add the attachment number to the draw buffers array
	m_buffers->push_back(GL_COLOR_ATTACHMENT0 + m_colourAttachments.size() - 1);

	//attach the image to the framebuffer
	glFramebufferTexture2D(GL_FRAMEBUFFER,
		m_buffers->back(),
		GL_TEXTURE_2D, att, 0);
}

void FBO::drawBuffers()
{
	assert(!m_buffers->empty());
	glDrawBuffers(m_buffers->size(), m_buffers->data());
	delete m_buffers;
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
