#pragma once
#include "FBO.h"
#include <matrix.hpp>
#include <glew.h>

class Camera;
struct GLFWwindow;

class Renderer
{
private:
	GLuint prepassProgram;
	GLuint geometryPassProgram;
	GLuint lightPassProgram;

	FBO deferredFrame;

	GLuint quad;
	GLuint model;
	GLuint modelSize;

	Camera *camera;

	void geometryPass();
	void lightPass();

public:
	Renderer();
	~Renderer();

	void init();
	void draw(GLFWwindow* window);
};

