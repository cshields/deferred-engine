#include "renderer.h"
#include "gl.h"
#include "Camera.h"
#include <gtc\matrix_transform.hpp>
#include <glfw3.h>



Renderer::Renderer()
{
	camera = new Camera(90.f, 16.f / 9.f, .1f, 100.f);
}


Renderer::~Renderer()
{
}

void Renderer::init()
{
	glEnable(GL_CULL_FACE);
	glClearColor(.5f, .5f, .5f, 1.f);

	prepassProgram = glt::initShader("shaders/prepass.vert", "shaders/prepass.frag");
	geometryPassProgram = glt::initShader("shaders/geometryPass.vert", "shaders/geometryPass.frag");
	lightPassProgram = glt::initShader("shaders/lightPass.vert", "shaders/lightPass.frag");

	quad = glt::generateQuad();
	model = glt::importMesh("models/bunny.obj", &modelSize);

	deferredFrame.createRenderBuffer(GL_DEPTH_COMPONENT, 1280, 720);
	deferredFrame.createAttachment("positionTarget", GL_RGBA32F, 1280, 720);
	deferredFrame.createAttachment("normalTarget", GL_RGB32F, 1280, 720);
	deferredFrame.createAttachment("colourTarget", GL_RGBA32F, 1280, 720);
	deferredFrame.drawBuffers();
}

void Renderer::draw(GLFWwindow* window)
{
	double xPos, yPos;
	while (!glfwWindowShouldClose(window))
	{
		geometryPass();
		lightPass();

		glfwSwapBuffers(window);

		/* Poll for and process events */
		glfwPollEvents();
		glfwGetCursorPos(window, &xPos, &yPos);
		camera->Orientate((float)xPos, (float)-yPos);
		camera->updateView();
	}
}


void Renderer::geometryPass()
{
	glBindFramebuffer(GL_FRAMEBUFFER, deferredFrame.getHandle());
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::mat4 modelMatrix;
	glm::mat4 modelView;
	glm::mat4 MVP;
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.f, 0.f, -5.f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(10.f));

	modelView = camera->getView() * modelMatrix;
	MVP = camera->getProjection() * modelView;

	// prepass writes only to the depth buffer
	glUseProgram(prepassProgram);
	glt::setUniform(prepassProgram, 0, MVP);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	glt::drawMesh(model, modelSize, GL_TRIANGLES);

	// geometry pass only writes out closest fragments
	glUseProgram(geometryPassProgram);
	glt::setUniform(prepassProgram, 0, MVP);
	glt::setUniform(prepassProgram, 1, modelView);
	glt::setUniform(prepassProgram, 2, glm::transpose(glm::inverse(glm::mat3(modelView))));

	// set depth function to GL_EQUAL or nothing will pass the depth test
	glDepthFunc(GL_EQUAL);

	glt::drawMesh(model, modelSize, GL_TRIANGLES);

}

void Renderer::lightPass()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClear(GL_COLOR_BUFFER_BIT);

	glUseProgram(lightPassProgram);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, deferredFrame.getAttachment("positionTarget"));
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, deferredFrame.getAttachment("normalTarget"));
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, deferredFrame.getAttachment("colourTarget"));

	glDisable(GL_DEPTH_TEST);

	glt::drawMesh(quad, 6, GL_TRIANGLES);

}